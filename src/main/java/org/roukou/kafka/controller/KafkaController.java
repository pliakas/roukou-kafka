package org.roukou.kafka.controller;

import static java.util.Objects.requireNonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.roukou.kafka.domain.KafkaMessage;
import org.roukou.kafka.domain.AdvancedKafkaMessage;
import org.roukou.kafka.domain.ResponseKafkaMessage;
import org.roukou.kafka.domain.SimpleKafkaMessage;
import org.roukou.kafka.service.AdvancedKafkaConsumerService;
import org.roukou.kafka.service.AdvancedKafkaProducerService;
import org.roukou.kafka.service.SimpleKafkaConsumerService;
import org.roukou.kafka.service.SimpleKafkaProducerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/kafka")
public class KafkaController {

  private final SimpleKafkaProducerService simpleKafkaProducerService;
  private final AdvancedKafkaProducerService advancedKafkaProducerService;
  private final AdvancedKafkaConsumerService advancedKafkaConsumerService;

  public KafkaController(
      SimpleKafkaProducerService simpleKafkaProducerService,
      SimpleKafkaConsumerService simpleKafkaConsumerService,
      AdvancedKafkaProducerService advancedKafkaProducerService,
      AdvancedKafkaConsumerService advancedKafkaConsumerService) {
    this.simpleKafkaProducerService = simpleKafkaProducerService;
    this.advancedKafkaProducerService = advancedKafkaProducerService;
    this.advancedKafkaConsumerService = advancedKafkaConsumerService;
  }

  @PostMapping(value = "/advanced")
  public ResponseKafkaMessage sendMessageToKafka(@RequestBody AdvancedKafkaMessage message)
      throws InterruptedException {
    requireNonNull(message, "Cannot be null.");
    try {
      // just initialize counters
      advancedKafkaConsumerService.initializeCounter();

      advancedKafkaProducerService.sendMessage(message);

    } catch (JsonProcessingException e) {
      log.error("Error Unable to convert message");
      e.printStackTrace();
    }

    Thread.sleep(10_000);
    return ResponseKafkaMessage.builder()
        .mainQueueMessageReceived(advancedKafkaConsumerService.getMainCounter().intValue())
        .retryQueueMessageReceived(advancedKafkaConsumerService.getRetryCounter().intValue())
        .build();
  }

  @PostMapping(value = "/simple")
  public ResponseKafkaMessage sendMessageToKafka(@RequestBody SimpleKafkaMessage message)
      throws InterruptedException {
    requireNonNull(message, "Cannot be null.");
    try {
      // just initialize counters

      simpleKafkaProducerService.sendMessage(message);

    } catch (JsonProcessingException e) {
      log.error("Error Unable to convert message");
      e.printStackTrace();
    }

    Thread.sleep(10_000);
    return ResponseKafkaMessage.builder()
        .mainQueueMessageReceived(message.getMessages())
        .build();
  }
}
