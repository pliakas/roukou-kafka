package org.roukou.kafka.kafka;

import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.InvalidRecordException;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;

@Log4j2
public class CustomPartition implements Partitioner {

  private String mainKey = "main-key";

  private String retryKey = "retry-key";

  @Override
  public void configure(Map<String, ?> configMap) {
    log.info("Configure custom partition = {}", () -> configMap);
  }

  @Override
  public int partition(
      String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {

    List<PartitionInfo> partitions = cluster.partitionsForTopic(topic);
    int numPartitions = partitions.size();

    // topics 30 % of partitions will be used for main
    int mainPartition = (int) Math.abs(numPartitions * 0.7);
    int partition = 0;

    if ((keyBytes == null) || (!(key instanceof String)))
      throw new InvalidRecordException("All messages must have key");

    if (((String) key).equals(mainKey)) {
      partition = Utils.toPositive(Utils.murmur2(valueBytes)) % mainPartition;
    } else {
      partition =
          Utils.toPositive(Utils.murmur2(keyBytes)) % (numPartitions - mainPartition)
              + mainPartition;
    }

    log.info("Key = {}, Partition = {}", key, partition);
    return partition;
  }

  @Override
  public void close() {}
}
