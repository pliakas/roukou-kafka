package org.roukou.kafka.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.roukou.kafka.domain.KafkaMessage;
import org.roukou.kafka.domain.AdvancedKafkaMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AdvancedKafkaProducerService {

  @Value("${spring.kafka.template.default-topic}")
  private String defaultTopic;

  private static final int NUMBER_OF_MESSAGES = 10;

  private final KafkaTemplate<String, String> kafkaTemplate;
  private final ObjectMapper objectMapper;

  public AdvancedKafkaProducerService(
      KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
    this.kafkaTemplate = kafkaTemplate;
    this.objectMapper = objectMapper;
  }

  public void sendMessage(AdvancedKafkaMessage message) throws JsonProcessingException {

    log.info(
        String.format(
            "$$$$ -> Producing %s messages for Main Queue -> %s", message.getMain(), message));
    for (int index = 0; index < message.getMain(); index++) {

      KafkaMessage dto =
          KafkaMessage.builder().greeting("Main Messages").name("message-main-" + index).build();
      kafkaTemplate.send(defaultTopic, "main-key", objectMapper.writeValueAsString(dto));
    }

    log.info(
        String.format(
            "$$$$ -> Producing %s Message for Retry Queue -> %s", message.getRetry(), message));
    for (int index = 0; index < message.getRetry(); index++) {
      KafkaMessage dto =
          KafkaMessage.builder().greeting("Retry Messages").name("message-retry-" + index).build();
      kafkaTemplate.send(defaultTopic, "retry-key-" + index, objectMapper.writeValueAsString(dto));
    }
  }
}
