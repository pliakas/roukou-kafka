package org.roukou.kafka.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SimpleKafkaConsumerService {

  private final ObjectMapper objectMapper;


  public SimpleKafkaConsumerService(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @KafkaListener(
      topicPartitions =
      @TopicPartition(
          topic = "roukou-simple-topic",
          partitions = {"0"}))
  public void listenPartitionZero(
      @Payload String message,
      @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey,
      @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    log.info("Listener Topic 0 -> Received Message: {} with key: {}, from partition: {}", message,
        messageKey,
        partition);
  }

  @KafkaListener(
      topicPartitions =
      @TopicPartition(
          topic = "roukou-simple-topic",
          partitions = {"1"}))
  public void listenPartitionOne(
      @Payload String message,
      @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey,
      @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    log.info("Listener Topic 1 -> Received Message: {} with key: {}, from partition: {}", message
        , messageKey,
        partition);
  }

  @KafkaListener(
      topicPartitions =
      @TopicPartition(
          topic = "roukou-simple-topic",
          partitions = {"2"}))
  public void listenPartitionTwo(
      @Payload String message,
      @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey,
      @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    log.info("Listener Topic 2 -> Received Message: {} with key: {}, from partition: {}", message
        , messageKey,
        partition);
  }
}
