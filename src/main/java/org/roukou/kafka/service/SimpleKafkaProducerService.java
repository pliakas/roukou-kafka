package org.roukou.kafka.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.roukou.kafka.domain.KafkaMessage;
import org.roukou.kafka.domain.SimpleKafkaMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class SimpleKafkaProducerService {

  private final KafkaTemplate<String, String> kafkaTemplate;
  private final ObjectMapper objectMapper;

  public SimpleKafkaProducerService(
      KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
    this.kafkaTemplate = kafkaTemplate;
    this.objectMapper = objectMapper;
  }

  public void sendMessage(SimpleKafkaMessage message) throws JsonProcessingException {


    for (int index = 0; index < message.getMessages(); index++) {
      KafkaMessage dto =
          KafkaMessage.builder().greeting("Hello Simple Message" ).name("message-index-" + index).build();

      String messageKey = null;

      if (index < 5) {
        messageKey = "message-key-number-one";
      } else if ( index >= 5 && index < 10) {
        messageKey = "message-key-number-two";
      } else {
        messageKey = "message-key-number-three";
      }


      log.info(
          String.format(
              "$$$$ -> Producing message number: %s, with key: %s and message: %s", index,
              messageKey,
              dto.toString()));


      kafkaTemplate.send("roukou-simple-topic", messageKey,
          objectMapper.writeValueAsString(dto));
    }
  }
}
