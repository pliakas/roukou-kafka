# Getting Started

## Kafka start up

```shell script
docker-compose -f zk-single-kafka-single.yml up -d
```

## Microservice startup
```shell script
./mvnw spring-boot:run
```

#### Prerequesites
You need java 11 or higher. 

## Description
One topic with "roukou-topic" with 10 partitions. The first 7 topics are used for sending messages with key="main-key" 
and the rest sending messages with key="retry-key". 

You can start using the following curl messages: 
```shell script
curl -X POST "http://localhost:8080/kafka/simple" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"mainQueueMessages\": 15, \"retryQueueMessages\": 5}"
```
Example sending 15 messages with key "main-key" and 5 messages with key "retry-key". Check the logs for details. 


### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/maven-plugin/)
* [Apache Kafka Streams Support](https://docs.spring.io/spring-kafka/docs/current/reference/html/_reference.html#kafka-streams)
* [Apache Kafka Streams Binding Capabilities of Spring Cloud Stream](https://docs.spring.io/spring-cloud-stream/docs/current/reference/htmlsingle/#_kafka_streams_binding_capabilities_of_spring_cloud_stream)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring for Apache Kafka](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/reference/htmlsingle/#boot-features-kafka)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/reference/htmlsingle/#using-boot-devtools)

### Guides
The following guides illustrate how to use some features concretely:

* [Samples for using Apache Kafka Streams with Spring Cloud stream](https://github.com/spring-cloud/spring-cloud-stream-samples/tree/master/kafka-streams-samples)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

